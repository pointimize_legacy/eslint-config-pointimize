# eslint-config-pointimize

ESLint config for Pointimize projects

## Installation

```bash
$ npm install eslint-config-pointimize --save-dev
```

### Required by pointimize/test config
```bash
$ npm install eslint-plugin-mocha --save-dev
```

## Usage

In `.eslintrc`

```js
{
  "extends": "pointimize"
}
```

## License

MIT
